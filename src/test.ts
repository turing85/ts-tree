import { Tree } from "./Tree";

type Consumer<T> = ({}: T) => void;
type AllConsumer = Consumer<unknown>;

run(console.log);

function run(print: AllConsumer): void {
    const tree: Tree<string> = new Tree("Marco", "Jutta", "Hans");
    print(tree.size());
    const content: string[] = new Array();
    tree.traverseInOrderAndApply((value: string) => content.push(value));
    print(content.join(", "));
}
