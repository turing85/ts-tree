type Consumer<T> = ({}: T) => void;
type TreeNodeConsumer<T> = Consumer<TreeNode<T>>;
type Optional<T> = T | undefined;

export class Tree<T> {
    private root: Optional<TreeNode<T>>;

    public constructor(...values: T[]) {
        this.add.apply(this, values);
    }

    public add(...values: T[]): void {
        const [first, ...rest] = values;
        if (first) {
            const newNode: TreeNode<T> = new TreeNode(first);
            if (!this.root) {
                this.root = newNode;
            } else {
                this.findPositionAndInsert(newNode, this.root);
            }
        }
        if (rest && rest.length !== 0) {
            this.add.apply(this, rest);
        }
    }

    private findPositionAndInsert(newNode: TreeNode<T>, currentNode: TreeNode<T>): void {
        let nextNode: Optional<TreeNode<T>>;
        let setter: TreeNodeConsumer<T>;

        if (currentNode.getValue() > newNode.getValue()) {
            nextNode = currentNode.getLeft();
            setter = currentNode.setLeft;
        } else {
            nextNode = currentNode.getRight();
            setter = currentNode.setRight;
        }

        if (!nextNode) {
            setter.call(currentNode, newNode);
            newNode.setParent(currentNode);
        } else {
            this.findPositionAndInsert(newNode, nextNode);
        }
    }

    public size(): number {
        let counter: number = 0;
        this.traverseInOrderAndApply(({}: T) => ++counter);
        return counter;
    }

    public traverseInOrderAndApply(consumer: Consumer<T>): void {
        if (this.root) {
            this.root.traverseInOrderAndApply(consumer);
        }
    }
}

class TreeNode<T> {
    private readonly value: T;
    private parent: Optional<TreeNode<T>>;
    private left: Optional<TreeNode<T>>;
    private right: Optional<TreeNode<T>>;

    constructor(value: T) {
        this.value = value;
    }

    public getValue(): T {
        return this.value;
    }

    public setParent(parent: TreeNode<T>): void {
        this.parent = parent;
    }

    public getParent(): Optional<TreeNode<T>> {
        return this.parent;
    }

    public setLeft(left: TreeNode<T>): void {
        this.left = left;
    }

    public getLeft(): Optional<TreeNode<T>> {
        return this.left;
    }

    public setRight(right: TreeNode<T>): void {
        this.right = right;
    }

    public getRight(): Optional<TreeNode<T>> {
        return this.right;
    }

    public traverseInOrderAndApply(consumer: Consumer<T>): void {
        const left: TreeNode<T> | undefined = this.getLeft();
        const right: TreeNode<T> | undefined = this.getRight();

        if (left) {
            left.traverseInOrderAndApply(consumer);
        }
        consumer(this.getValue());
        if (right) {
            right.traverseInOrderAndApply(consumer);
        }
    }
}
